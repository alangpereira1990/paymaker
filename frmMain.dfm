object Main: TMain
  Left = 0
  Top = 0
  Caption = 'Payment Maker'
  ClientHeight = 519
  ClientWidth = 810
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 810
    Height = 73
    Margins.Bottom = 0
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 801
    DesignSize = (
      810
      73)
    object Label1: TLabel
      Left = 295
      Top = 13
      Width = 82
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Sal'#225'rio Base (R$)'
      ExplicitLeft = 286
    end
    object Label2: TLabel
      Left = 503
      Top = 13
      Width = 62
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Horas Extras'
      ExplicitLeft = 512
      ExplicitTop = 117
    end
    object Label3: TLabel
      Left = 612
      Top = 13
      Width = 19
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = '(%)'
      ExplicitLeft = 621
      ExplicitTop = 117
    end
    object Label4: TLabel
      Left = 662
      Top = 13
      Width = 123
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Faltas e Atrasos (HH:MM)'
      ExplicitLeft = 653
    end
    object Label10: TLabel
      Left = 391
      Top = 13
      Width = 78
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Jornada (Horas)'
      ExplicitLeft = 382
    end
    object edtSalario: TMaskEdit
      Left = 311
      Top = 32
      Width = 66
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      EditMask = '##.##0,00;1;_'
      MaxLength = 9
      TabOrder = 0
      Text = '  .   ,  '
      ExplicitLeft = 302
    end
    object edtQtdHExtra: TMaskEdit
      Left = 503
      Top = 32
      Width = 58
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 2
      Text = '  :  '
      ExplicitLeft = 494
    end
    object edtPercHExtra: TMaskEdit
      Left = 571
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      EditMask = '!999,99;1;_'
      MaxLength = 6
      TabOrder = 3
      Text = '   ,  '
      ExplicitLeft = 562
    end
    object edtAtrasos: TMaskEdit
      Left = 725
      Top = 32
      Width = 58
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 4
      Text = '  :  '
      ExplicitLeft = 716
    end
    object edtJornada: TMaskEdit
      Left = 399
      Top = 32
      Width = 68
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      EditMask = '!999;1;_'
      MaxLength = 3
      TabOrder = 1
      Text = '   '
      ExplicitLeft = 390
    end
  end
  object grdDetalhe: TStringGrid
    Left = 0
    Top = 105
    Width = 810
    Height = 280
    Cursor = crArrow
    Align = alTop
    ColCount = 4
    DefaultDrawing = False
    FixedCols = 0
    RowCount = 10
    FixedRows = 9
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnDrawCell = grdDetalheDrawCell
    ExplicitWidth = 801
    ColWidths = (
      542
      64
      99
      98)
  end
  object Panel2: TPanel
    Left = 0
    Top = 385
    Width = 810
    Height = 134
    Align = alClient
    TabOrder = 2
    ExplicitTop = 391
    DesignSize = (
      810
      134)
    object Label5: TLabel
      Left = 600
      Top = 20
      Width = 74
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Descontos (R$)'
    end
    object Label6: TLabel
      Left = 505
      Top = 20
      Width = 49
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'FGTS (R$)'
    end
    object Label7: TLabel
      Left = 709
      Top = 20
      Width = 73
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Proventos (R$)'
    end
    object Label8: TLabel
      Left = 701
      Top = 76
      Width = 84
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Total L'#237'quido (R$)'
    end
    object Label9: TLabel
      Left = 16
      Top = 16
      Width = 180
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Desenvolvido por Alan Gabriel Pereira'
    end
    object Label11: TLabel
      Left = 16
      Top = 48
      Width = 283
      Height = 13
      Caption = 'Fonte: http://contabnet.com.br/blog/folha-de-pagamento/'
    end
    object edtDescontos: TMaskEdit
      Left = 600
      Top = 39
      Width = 70
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      ReadOnly = True
      TabOrder = 1
      Text = ''
    end
    object edtFGTS: TMaskEdit
      Left = 484
      Top = 39
      Width = 70
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      ReadOnly = True
      TabOrder = 0
      Text = ''
    end
    object edtProventos: TMaskEdit
      Left = 712
      Top = 39
      Width = 70
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      ReadOnly = True
      TabOrder = 2
      Text = ''
    end
    object edtTotalLiq: TMaskEdit
      Left = 712
      Top = 95
      Width = 70
      Height = 21
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      ReadOnly = True
      TabOrder = 3
      Text = ''
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 73
    Width = 810
    Height = 32
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    TabOrder = 3
    object btnCalc: TButton
      Left = 685
      Top = -6
      Width = 175
      Height = 41
      Cursor = crHandPoint
      Caption = 'Calcular'
      Style = bsCommandLink
      TabOrder = 0
      OnClick = btnCalcClick
    end
  end
end
