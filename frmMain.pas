unit frmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Mask, System.Math;

type
  TMain = class(TForm)
    Panel1: TPanel;
    grdDetalhe: TStringGrid;
    Panel2: TPanel;
    Label1: TLabel;
    edtSalario: TMaskEdit;
    edtQtdHExtra: TMaskEdit;
    Label2: TLabel;
    edtPercHExtra: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtAtrasos: TMaskEdit;
    edtDescontos: TMaskEdit;
    Label5: TLabel;
    edtFGTS: TMaskEdit;
    Label6: TLabel;
    edtProventos: TMaskEdit;
    Label7: TLabel;
    edtTotalLiq: TMaskEdit;
    Label8: TLabel;
    Panel3: TPanel;
    btnCalc: TButton;
    Label9: TLabel;
    edtJornada: TMaskEdit;
    Label10: TLabel;
    Label11: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnCalcClick(Sender: TObject);
    procedure grdDetalheDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
    procedure Calcular;
  public
    { Public declarations }
  end;

var
  Main: TMain;

const
  ColDescricao: Integer = 0;
  ColReferencia: Integer = 1;
  ColValorProvento: Integer = 2;
  ColValorDesconto: Integer = 3;
  RowCabecalho: Integer = 0;

implementation

{$R *.dfm}

procedure TMain.btnCalcClick(Sender: TObject);
begin
  Calcular;
end;

procedure TMain.Calcular;
var
  salarioBase, qtdHoraExtra, percHoraExtra, faltasAtrasos,
  valorHora, valorDSR, horasAtraso, valorINSS,
  aliqIRRF, baseIRRF, deducaoIRRF, valorIRRF,
  TotalProventos, TotalDescontos: Double;
  jornada, diasFalta: Integer;
  gridRow, aliqINSS: Integer;
begin
  gridRow := 1;
  TotalProventos := 0;
  TotalDescontos := 0;

  salarioBase := StrToFloat(StringReplace(edtSalario.Text,'.','',[rfReplaceAll]));
  if salarioBase <> 0 then
  begin
    grdDetalhe.Cells[ColDescricao,gridRow] := 'Sal�rio Base';
    grdDetalhe.Cells[ColReferencia,gridRow] := '100,00';
    grdDetalhe.Cells[ColValorProvento,gridRow] := FormatFloat('R$ ###,###,##0.00',salarioBase);
    TotalProventos := TotalProventos + salarioBase;
    inc(gridRow);
  end;

  jornada := StrToInt(edtJornada.Text);
  qtdHoraExtra := RoundTo(24 * StrToTime(edtQtdHExtra.Text),-2);
  percHoraExtra := StrToFloat(edtPercHExtra.Text);
  faltasAtrasos := RoundTo(24 * StrToTime(edtAtrasos.Text),-2);

  valorHora := salarioBase / jornada;
  valorHora := valorHora + (valorHora * (percHoraExtra/100));
  valorHora := RoundTo(valorHora * qtdHoraExtra,-2);

  if valorHora <> 0 then
  begin
    grdDetalhe.Cells[ColDescricao,gridRow] := 'Horas Extras';
    grdDetalhe.Cells[ColReferencia,gridRow] := FloatToStr(qtdHoraExtra);
    grdDetalhe.Cells[ColValorProvento,gridRow] := FormatFloat('R$ ###,###,##0.00',valorHora);
    TotalProventos := TotalProventos + valorHora;
    Inc(gridRow);

    valorDSR := valorHora / 26;
    valorDSR := valorDSR * 4;
    grdDetalhe.Cells[ColDescricao,gridRow] := 'D.S.R. Sobre Horas Extras';
    grdDetalhe.Cells[ColValorProvento,gridRow] := FormatFloat('R$ ###,###,##0.00',valorDSR);
    TotalProventos := TotalProventos + valorDSR;
    Inc(gridRow);
  end;

  if faltasAtrasos > 0 then
  begin
    if faltasAtrasos > 8 then
    begin
      diasFalta := Trunc(faltasAtrasos/8);
      horasAtraso := faltasAtrasos - (diasFalta*8);

      grdDetalhe.Cells[ColDescricao,gridRow] := 'Faltas Injustificadas';
      grdDetalhe.Cells[ColReferencia,gridRow] := FloatToStr(diasFalta);
      grdDetalhe.Cells[ColValorDesconto,gridRow] := FormatFloat('R$ ###,###,##0.00',((salarioBase / jornada)*8.46*diasFalta));
      TotalDescontos := TotalDescontos + ((salarioBase / jornada)*8.46*diasFalta);
      Inc(gridRow);

      grdDetalhe.Cells[ColDescricao,gridRow] := 'D.S.R. Sobre Faltas Injustificadas';
      grdDetalhe.Cells[ColValorDesconto,gridRow] := FormatFloat('R$ ###,###,##0.00',((salarioBase/30)*diasFalta));
      TotalDescontos := TotalDescontos + ((salarioBase/30)*diasFalta);
      Inc(gridRow);

      grdDetalhe.Cells[ColDescricao,gridRow] := 'Atrasos Injustificados';
      grdDetalhe.Cells[ColReferencia,gridRow] := FloatToStr(horasAtraso);
      grdDetalhe.Cells[ColValorDesconto,gridRow] := FormatFloat('R$ ###,###,##0.00',((salarioBase / jornada)*horasAtraso));
      Inc(gridRow);
    end
    else
    begin
      horasAtraso := faltasAtrasos;
      grdDetalhe.Cells[ColDescricao,gridRow] := 'Atrasos Injustificados';
      grdDetalhe.Cells[ColReferencia,gridRow] := FloatToStr(horasAtraso);
      grdDetalhe.Cells[ColValorDesconto,gridRow] := FormatFloat('R$ ###,###,##0.00',((salarioBase / jornada)*horasAtraso));
      TotalDescontos := TotalDescontos + ((salarioBase / jornada)*horasAtraso);
      Inc(gridRow);
    end;
  end;

  if (salarioBase + valorHora) > 5531.31 then
  begin
    aliqINSS := 11;
    valorINSS := 608.44;
  end
  else
  begin
    if (salarioBase + valorHora) <= 1659.38 then
      aliqINSS := 8
    else if (salarioBase + valorHora) <= 2765.66 then
      aliqINSS := 9
    else if (salarioBase + valorHora) <= 5531.31 then
      aliqINSS := 11;

    valorINSS := (salarioBase + valorHora) * (aliqINSS/100);
  end;
  grdDetalhe.Cells[ColDescricao,gridRow] := 'INSS Sobre Sal�rio';
  grdDetalhe.Cells[ColReferencia,gridRow] := FloatToStr(aliqINSS);
  grdDetalhe.Cells[ColValorDesconto,gridRow] := FormatFloat('R$ ###,###,##0.00',valorINSS);
  TotalDescontos := TotalDescontos + valorINSS;
  Inc(gridRow);

  baseIRRF := (salarioBase + valorHora) - valorINSS;
  if baseIRRF < 1903.99 then
  begin
    deducaoIRRF := 0;
    aliqIRRF := 0;
  end
  else if baseIRRF <= 2826.65 then
  begin
    deducaoIRRF := 142.80;
    aliqIRRF := 7.5;
  end
  else if baseIRRF <= 3751.05 then
  begin
    deducaoIRRF := 354.80;
    aliqIRRF := 15;
  end
  else if baseIRRF <= 4664.68 then
  begin
    deducaoIRRF := 636.13;
    aliqIRRF := 22.5;
  end
  else
  begin
    deducaoIRRF := 869.36;
    aliqIRRF := 27.5;
  end;
  valorIRRF := (baseIRRF * (aliqIRRF / 100)) - deducaoIRRF;

  grdDetalhe.Cells[ColDescricao,gridRow] := 'IRRF Sobre Sal�rio';
  grdDetalhe.Cells[ColReferencia,gridRow] := FloatToStr(aliqIRRF);
  grdDetalhe.Cells[ColValorDesconto,gridRow] := FormatFloat('R$ ###,###,##0.00',valorIRRF);
  TotalDescontos := TotalDescontos + valorIRRF;
  Inc(gridRow);

  edtProventos.Text := FormatFloat('R$ ###,###,##0.00',TotalProventos);
  edtDescontos.Text := FormatFloat('R$ ###,###,##0.00',TotalDescontos);
  edtFGTS.Text := FormatFloat('R$ ###,###,##0.00',salarioBase*0.08);
  edtTotalLiq.Text := FormatFloat('R$ ###,###,##0.00',TotalProventos - TotalDescontos);
end;

procedure TMain.FormCreate(Sender: TObject);
begin
  grdDetalhe.Cells[ColDescricao,RowCabecalho] := 'Descri��o';
  grdDetalhe.Cells[ColReferencia,RowCabecalho] := 'Refer�ncia';
  grdDetalhe.Cells[ColValorProvento,RowCabecalho] := 'Proventos (R$)';
  grdDetalhe.Cells[ColValorDesconto,RowCabecalho] := 'Descontos (R$)';
end;

procedure TMain.grdDetalheDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  with grdDetalhe,Canvas do
  begin
    Font.Color := clBlack;
    if ARow = RowCabecalho then
    begin
      Font.Style := [fsBold]
    end
    else
    begin
      if ACol = ColValorProvento then
        Font.Color := clBlue
      else if ACol = ColValorDesconto then
        Font.Color := clRed;
    end;

    TextRect(Rect, Rect.Left + 2, Rect.Top + 2, Cells[ACol, ARow]);
  end;
end;

initialization
  FormatSettings.decimalSeparator := ',';

end.


